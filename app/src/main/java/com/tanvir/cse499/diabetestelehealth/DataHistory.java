package com.tanvir.cse499.diabetestelehealth;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.LegendEntry;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.tanvir.cse499.diabetestelehealth.models.Bglevel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DataHistory extends AppCompatActivity {

    private ArrayList<Bglevel> bg_levels= new ArrayList<>();
    private static final String TAG = "DataHistory";
    private BarChart barChart;
    private Button feedback;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_history);
        barChart= (BarChart) findViewById(R.id.barchart);
        feedback= (Button) findViewById(R.id.feedbackFromData);

        DatabaseReference ref= FirebaseDatabase.getInstance().getReference();
        Query query= ref.child("users").child(getIntent().getStringExtra("patient_id"))
                .child("bg_level");
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                try {
                    Map<String,Object> bg_level_list;
                    for(DataSnapshot snapShot:dataSnapshot.getChildren()){
                        Bglevel bglevel= new Bglevel();
                        bg_level_list= (HashMap<String, Object>)snapShot.getValue();
                        bglevel.setBglevelId(bg_level_list.get("bglevelId").toString());
                        bglevel.setBglevel(bg_level_list.get("bglevel").toString());
                        bglevel.setTimestamp(bg_level_list.get("timestamp").toString());
                        bg_levels.add(bglevel);
                    }

                    ArrayList<BarEntry> barEntries=new ArrayList<>();
                    int index=0;
                    try{
                        for(Bglevel bglevel: bg_levels)
                        {
                            barEntries.add(new BarEntry(index,Float.parseFloat(bglevel.getBglevel())));
                            index++;
                        }
                        Toast.makeText(getApplicationContext(),String.valueOf(index),Toast.LENGTH_SHORT).show();
                        Log.d(TAG, "onDataChange: "+index);
                        MyBarDataSet barDataSet= new MyBarDataSet(barEntries,"Glucose Level");

                        Log.d(TAG, "onDataChange: BarEntry Size"+barEntries.size());
                        // String[] timestamps = new String[bg_levels.size()];

                        ArrayList<String> timestamps= new ArrayList<>();


                        for (Bglevel timestamp:bg_levels) {
                            timestamps.add(timestamp.getTimestamp());
                        }


                        barChart.getXAxis().setValueFormatter(new LabelFormatter(timestamps));
                        int colors[]=new int[]{ContextCompat.getColor(DataHistory.this,android.R.color.holo_green_dark),
                                ContextCompat.getColor(DataHistory.this,android.R.color.holo_orange_dark),
                                ContextCompat.getColor(DataHistory.this,android.R.color.holo_red_dark)};
                        String labels[]= new String[]{
                            "Glucose level Normal",
                            "Glucose level High",
                            "Glucose level fatal"
                        };

                        List<LegendEntry> legendEntryList= new ArrayList<>();
                        for(int i=0;i<labels.length;i++){
                            LegendEntry legendEntry= new LegendEntry();
                            legendEntry.formColor= colors[i];
                            legendEntry.label=labels[i];
                            legendEntryList.add(legendEntry);
                        }
                        barDataSet.setColors(colors);

                        barDataSet.setDrawIcons(true);
                        BarData barData= new BarData(barDataSet);
                        Legend legend=barChart.getLegend();
                        legend.setHorizontalAlignment(Legend.LegendHorizontalAlignment.RIGHT);
                        legend.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
                        legend.setTextColor(Color.WHITE);
                        legend.setTextSize(10f);
                        legend.setCustom(legendEntryList);
                        barChart.getXAxis().setTextColor(Color.WHITE);
                        barChart.getXAxis().setLabelRotationAngle(-90);
                        barChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
                        barData.setValueTextColor(Color.WHITE);
                        barChart.getAxisLeft().setTextColor(Color.WHITE);
                        barChart.getAxisRight().setTextColor(Color.WHITE);
                        barChart.setFitBars(true);
                        barChart.setData(barData);
                        XAxis x= barChart.getXAxis();
                        x.setDrawGridLines(false);
                        x.setCenterAxisLabels(false);
                        x.setGranularity(1f);
                        barChart.getDescription().setEnabled(false);
                        barChart.setScaleEnabled(true);
                        barChart.setDragEnabled(true);
                        barChart.setScaleEnabled(true);
                        barChart.setVisibleXRangeMaximum(7f);
                        barChart.animateY(1500);
                        barChart.invalidate();
                    }catch (Exception e){
                        Toast.makeText(DataHistory.this,e.toString(),Toast.LENGTH_LONG).show();
                    }
                }catch (NullPointerException e){
                    Toast.makeText(DataHistory.this,"You don't have any data",Toast.LENGTH_LONG).show();
                }


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        /*
            For Doctor part only
         */

        if(!getIntent().getStringExtra("patient_id").isEmpty()){
            feedback.setVisibility(View.VISIBLE);
            feedback.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(DataHistory.this,SendFeedBack.class);
                    intent.putExtra("patient_id",getIntent().getStringExtra("patient_id"));
                    startActivity(intent);
                }
            });
        }
    }

    /*
     Formats the timestamps in the x axis
     */

    public class LabelFormatter implements IAxisValueFormatter {
        private  ArrayList<String> mlabels;

        public LabelFormatter(ArrayList<String > mlabels) {
            this.mlabels = mlabels;
        }

        @Override
        public String getFormattedValue(float value, AxisBase axis) {
            try {
                int index = (int) value;
                return mlabels.get(index);
            } catch (Exception e) {
                return "";
            }
        }
    }

    /*
        This class defines the barchart legend and it's colors

     */

    public class MyBarDataSet extends BarDataSet {

        List<BarEntry> barEntries;

        public MyBarDataSet(List<BarEntry> yVals, String label) {
            super(yVals, label);
            barEntries= yVals;
        }

        @Override
        public int getColor(int index) {
            if(barEntries.get(index).getY() < 130) // green
                return mColors.get(0);
            else if(barEntries.get(index).getY() < 135) //orange
                return mColors.get(1);
            else
                return mColors.get(2); //red
        }

    }
}
