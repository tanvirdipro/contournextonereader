package com.tanvir.cse499.diabetestelehealth.utils;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.tanvir.cse499.diabetestelehealth.DoctorList;
import com.tanvir.cse499.diabetestelehealth.R;
import com.tanvir.cse499.diabetestelehealth.models.Hospital;

import java.util.List;

public class HospitalListAdapter extends RecyclerView.Adapter<HospitalListAdapter.HospitalViewHolder> {

    List<Hospital> hospitals;
    private Context context;
    private static final String TAG = "HospitalListAdapter";

    public HospitalListAdapter() {
    }

    public HospitalListAdapter(List<Hospital> hospitals, Context context)
    {
        this.hospitals = hospitals;
        this.context=context;
    }

    @Override
    public HospitalViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater=LayoutInflater.from(parent.getContext());
        View view= layoutInflater.inflate(R.layout.custom_row_item, parent, false);
        return new HospitalViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final HospitalViewHolder holder, final int position) {

        Picasso.get().load("https://fancydistrict.com/upload/media/posts/2018-01/15/08244b0e170c9d934da613c78765612a_1516013758-b.jpg").into(holder.hospitalImage);
        holder.hospitalName.setText("Hospital Name: "+ hospitals.get(position).getHospital_name());
        holder.hospitalLocation.setText("Location :"+ hospitals.get(position).getHospital_location());

        holder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(context,DoctorList.class);
                intent.putExtra("Hospital id", hospitals.get(position).getHospital_id());
                intent.putExtra("Hospital name",hospitals.get(position).getHospital_name());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return hospitals.size();
    }

    public class HospitalViewHolder extends RecyclerView.ViewHolder {

        TextView hospitalName,hospitalLocation;
        CardView parentLayout;
        ImageView hospitalImage;
        public HospitalViewHolder(View itemView) {
            super(itemView);
            parentLayout= (CardView) itemView.findViewById(R.id.cardViewHospital);
            hospitalName=(TextView)itemView.findViewById(R.id.hospitalName);
            hospitalLocation=(TextView)itemView.findViewById(R.id.hospitalLocation);
            hospitalImage=(ImageView)itemView.findViewById(R.id.hospitalImage);

        }
    }

}
