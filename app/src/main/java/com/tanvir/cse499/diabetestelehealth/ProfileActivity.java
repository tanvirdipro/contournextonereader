package com.tanvir.cse499.diabetestelehealth;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;
import com.tanvir.cse499.diabetestelehealth.models.Assign;
import com.tanvir.cse499.diabetestelehealth.models.Doctor;
import com.tanvir.cse499.diabetestelehealth.models.Hospital;
import com.tanvir.cse499.diabetestelehealth.models.User;

public class ProfileActivity extends AppCompatActivity {

    private static final String TAG = "ProfileActivity";
    private DatabaseReference ref = FirebaseDatabase.getInstance().getReference();
    private static final String NULL_VALUE="";
    private User user=new User();
    private Doctor doctor= new Doctor();
    private TextView docName,docClinic,docDesignation,docSpecialization,name,email,phoneNo,age,spouseName;
    private CardView cardView;
    private ImageView dltBtn,docImage;
    private Button update;
    private TextView title;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile2);
        name = (TextView) findViewById(R.id.userNameView);
        email = (TextView) findViewById(R.id.userMailView);
        phoneNo = (TextView) findViewById(R.id.userPhoneView);
        age = (TextView) findViewById(R.id.userAgeView);
        spouseName = (TextView)findViewById(R.id.userSpouseNameView);
        update = (Button) findViewById(R.id.editInformation);
        title = (TextView) findViewById(R.id.cardTitle);

        cardView = (CardView) findViewById(R.id.cardView);
        dltBtn = (ImageView)findViewById(R.id.dltDoc);
        docImage= (ImageView) findViewById(R.id.doc_image);

        docName = (TextView) findViewById(R.id.doc_name);
        docClinic = (TextView) findViewById(R.id.doc_clinic);
        docDesignation = (TextView) findViewById(R.id.doc_designation);
        docSpecialization = (TextView) findViewById(R.id.doc_specialization);

        Query query = ref.child("users")
                .orderByKey()
                .equalTo(FirebaseAuth.getInstance().getCurrentUser().getUid());
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot singleSnapshot: dataSnapshot.getChildren()){
                    user=singleSnapshot.getValue(User.class);
                    name.setText(user.getName());
                    email.setText(FirebaseAuth.getInstance().getCurrentUser().getEmail());
                    phoneNo.setText(user.getPhone_number());
                    age.setText(user.getAge());
                    spouseName.setText(user.getSpouse_name());
                    if(user.getDoctor_id()==NULL_VALUE)
                        cardView.setVisibility(View.INVISIBLE);
                    else{
                        getAssignedDoctor();
                        autoUpdateDoctorRequest();
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(getApplicationContext(),"User"+databaseError.toString(),Toast.LENGTH_SHORT).show();
            }
        });

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ProfileActivity.this,ProfileEditActivity.class));
            }
        });




    }

    ValueEventListener valueEventListener= new ValueEventListener() {
        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            getAssignedDoctor();
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {

        }
    };

    private void autoUpdateDoctorRequest(){
        ref= FirebaseDatabase.getInstance().getReference()
                .child("assign");
        ref.addValueEventListener(valueEventListener);
    }

    private void getAssignedDoctor(){
        final DatabaseReference ref = FirebaseDatabase.getInstance().getReference();
        Query queryAssign= ref.child("assign")
                .orderByKey().equalTo(user.getDoctor_id());
        queryAssign.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot singleSnapshot: dataSnapshot.getChildren()) {
                    final Assign assign= singleSnapshot.getValue(Assign.class);
                    Query query2 = ref.child("doctors")
                            .orderByKey()
                            .equalTo(assign.getDoctor_id());
                    query2.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            for(DataSnapshot singleSnapshot: dataSnapshot.getChildren()){
                                doctor=singleSnapshot.getValue(Doctor.class);
                                Log.d(TAG, "onDataChange: Doctor data loading "+ user.getDoctor_id());
                                Query query3=ref.child("hospitals")
                                        .orderByChild("hospital_id")
                                        .equalTo(doctor.getHospital_id());
                                query3.addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                        for(DataSnapshot snap: dataSnapshot.getChildren()){
                                            Hospital hospital =  snap.getValue(Hospital.class);
                                            dltBtn.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    AlertDialog.Builder removeDocDialog= new AlertDialog.Builder(ProfileActivity.this);
                                                    removeDocDialog.setTitle("Confirmation");
                                                    removeDocDialog.setMessage("Are you sure you want to remove this doctor?");
                                                    removeDocDialog.setPositiveButton("Delete",
                                                            new DialogInterface.OnClickListener() {
                                                                @Override
                                                                public void onClick(DialogInterface dialog, int which) {
                                                                    dltDoc(assign.getAssign_id());
                                                                    cardView.setVisibility(View.INVISIBLE);
                                                                }
                                                            });
                                                    removeDocDialog.setNegativeButton("Cancel",
                                                            new DialogInterface.OnClickListener() {
                                                                @Override
                                                                public void onClick(DialogInterface dialog, int which) {
                                                                }
                                                            });

                                                    removeDocDialog.create().show();
                                                }
                                            });

                                            Picasso.get().load("https://img.freepik.com/free-vector/doctor-character-background_1270-84.jpg?size=338&ext=jpg").into(docImage);
                                            if(assign.getIsAccepted().equals("true")){
                                                title.setText("Assigned Doctor");
                                                Picasso.get().load("https://cdn1.iconfinder.com/data/icons/round-ui/123/47-512.png").into(dltBtn);
                                                Toast.makeText(ProfileActivity.this, "Request Accepted", Toast.LENGTH_SHORT).show();
                                            }

                                            else if(assign.getIsAccepted().equals("false")){
                                                title.setText("Requested Doctor");
                                                Picasso.get().load("https://upload.wikimedia.org/wikipedia/commons/thumb/9/90/Stop_hand_caution.svg/2000px-Stop_hand_caution.svg.png").into(dltBtn);
                                                Toast.makeText(ProfileActivity.this, "Request Pending", Toast.LENGTH_SHORT).show();
                                            }
                                            docName.setText("Name:"+doctor.getName());
                                            docClinic.setText("Hospital: "+hospital.getHospital_name());
                                            docDesignation.setText("Designation: "+doctor.getDesignation());
                                            docSpecialization.setText("Specialization: "+doctor.getSpecialization());
                                            cardView.setVisibility(View.VISIBLE);
                                        }
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {
                                        Toast.makeText(getApplicationContext(),databaseError.toString(),Toast.LENGTH_SHORT).show();
                                    }
                                });
                                Log.d(TAG, "DoctorInfo: "+doctor.toString());
                            }

                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                            Toast.makeText(getApplicationContext(),"Doctor"+databaseError.toString(),Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void dltDoc(String assignId){
        FirebaseDatabase.getInstance().getReference()
                .child("users").child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .child("doctor_id")
                .setValue(NULL_VALUE);
        FirebaseDatabase.getInstance().getReference()
                .child("assign")
                .child(assignId)
                .removeValue();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ref.removeEventListener(valueEventListener);
        Log.d(TAG, "onDestroy: Value listener removed");

    }
}
