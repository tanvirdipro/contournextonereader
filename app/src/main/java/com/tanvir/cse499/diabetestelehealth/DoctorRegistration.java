package com.tanvir.cse499.diabetestelehealth;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.tanvir.cse499.diabetestelehealth.models.Bglevel;
import com.tanvir.cse499.diabetestelehealth.models.Doctor;
import com.tanvir.cse499.diabetestelehealth.models.DoctorMsg;
import com.tanvir.cse499.diabetestelehealth.models.User;

public class DoctorRegistration extends AppCompatActivity {

    private static final String NULL_VALUE="";
    private EditText email,password;
    private Button signUp,login;
    private FirebaseAuth firebaseAuth;
    private ProgressBar progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor_registration);
        progressBar= (ProgressBar) findViewById(R.id.dRegBarLogin);
        email= (EditText) findViewById(R.id.doctorRegEmail);
        password= (EditText) findViewById(R.id.doctorRegPassword);
        signUp= (Button) findViewById(R.id.dREGloginBtn);
        login= (Button) findViewById(R.id.doctorLogin);

        ConnectivityManager connectivityManager= (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo= connectivityManager.getActiveNetworkInfo();
        final boolean isConnected= networkInfo!=null && networkInfo.isConnectedOrConnecting();

        firebaseAuth=FirebaseAuth.getInstance();
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(DoctorRegistration.this,DoctorLogin.class));
                finish();
            }
        });

        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String uMail= email.getText().toString();
                String uPass= password.getText().toString();

                if(isConnected)
                {
                    if (TextUtils.isEmpty(uMail) || TextUtils.isEmpty(uPass))
                        Toast.makeText(getApplicationContext(),"Fill Up the credentials",Toast.LENGTH_LONG).show();

                    if(uPass.length()<6)
                        Toast.makeText(getApplicationContext(),"Password too short",Toast.LENGTH_LONG).show();

                    else{
                        // creates new user in firebase
                        progressBar.setVisibility(View.VISIBLE);
                        registerNewEmail(uMail,uPass);
                    }
                }
                else
                    Toast.makeText(getApplicationContext(),"Turn on Internet",Toast.LENGTH_LONG).show();

            }
        });
    }

    private void registerNewEmail(final String email, String pass)
    {
        FirebaseAuth.getInstance().createUserWithEmailAndPassword(email,pass)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            progressBar.setVisibility(View.INVISIBLE);

                            Doctor doctor= new Doctor();

                            doctor.setHospital_id("aslfhalfhlakhsfklahflaksfhklaf");
                            doctor.setDoctor_email(firebaseAuth.getCurrentUser().getEmail());
                            doctor.setDoctor_id(firebaseAuth.getCurrentUser().getUid());
                            doctor.setDoctor_phone("01478965");
                            doctor.setSpecialization("Heart Specialist");
                            doctor.setDesignation("Surgeon");
                            doctor.setName("Test Doctor");
                            doctor.setIsApproved("true");



                            FirebaseDatabase.getInstance().getReference()
                                    .child("doctors")
                                    .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                                    .setValue(doctor);
                            FirebaseAuth.getInstance().signOut();
                            startActivity(new Intent(DoctorRegistration.this,DoctorLogin.class));
                            finish();

                        }
                        else
                            displayToast(task.getException().getMessage());
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                progressBar.setVisibility(View.INVISIBLE);
            }
        });
    }

    private void displayToast(String msg){
        Toast.makeText(getApplicationContext(),msg,Toast.LENGTH_LONG).show();
    }
}
