package com.tanvir.cse499.diabetestelehealth.models;

public class Doctor {
    private String designation;
    private String doctor_email;
    private String doctor_id;
    private String doctor_phone;
    private String hospital_id;
    private String name;
    private String specialization;
    private String isApproved;

    public Doctor(String designation, String doctor_email, String doctor_id, String doctor_phone, String hospital_id, String name, String specialization, String isApproved) {
        this.designation = designation;
        this.doctor_email = doctor_email;
        this.doctor_id = doctor_id;
        this.doctor_phone = doctor_phone;
        this.hospital_id = hospital_id;
        this.name = name;
        this.specialization = specialization;
        this.isApproved = isApproved;
    }

    public Doctor() {
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getDoctor_email() {
        return doctor_email;
    }

    public void setDoctor_email(String doctor_email) {
        this.doctor_email = doctor_email;
    }

    public String getDoctor_id() {
        return doctor_id;
    }

    public void setDoctor_id(String doctor_id) {
        this.doctor_id = doctor_id;
    }

    public String getDoctor_phone() {
        return doctor_phone;
    }

    public void setDoctor_phone(String doctor_phone) {
        this.doctor_phone = doctor_phone;
    }

    public String getHospital_id() {
        return hospital_id;
    }

    public void setHospital_id(String hospital_id) {
        this.hospital_id = hospital_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSpecialization() {
        return specialization;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }

    public String getIsApproved() {
        return isApproved;
    }

    public void setIsApproved(String isApproved) {
        this.isApproved = isApproved;
    }

    @Override
    public String toString() {
        return "Doctor{" +
                "designation='" + designation + '\'' +
                ", doctor_email='" + doctor_email + '\'' +
                ", doctor_id='" + doctor_id + '\'' +
                ", doctor_phone='" + doctor_phone + '\'' +
                ", hospital_id='" + hospital_id + '\'' +
                ", name='" + name + '\'' +
                ", specialization='" + specialization + '\'' +
                ", isApproved='" + isApproved + '\'' +
                '}';
    }
}
