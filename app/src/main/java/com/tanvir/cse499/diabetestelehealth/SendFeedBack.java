package com.tanvir.cse499.diabetestelehealth;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.FirebaseDatabase;
import com.tanvir.cse499.diabetestelehealth.models.Feedback;

import java.text.SimpleDateFormat;
import java.util.Date;

public class SendFeedBack extends AppCompatActivity {

    private EditText msg,name;
    private Button send;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_feed_back);
        msg= (EditText) findViewById(R.id.doctorFeedback);
        send = (Button) findViewById(R.id.sendFeedback);
        name = (EditText) findViewById(R.id.feebackDoctorName);

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Feedback feedback= new Feedback();
                String feedbackId = FirebaseDatabase.getInstance().getReference()
                        .child("feedbacks")
                        .push().getKey();
                feedback.setPatient_id(getIntent().getStringExtra("patient_id"));
                feedback.setFeedback_id(feedbackId);
                feedback.setDoctor_name(name.getText().toString());
                feedback.setFeedback(msg.getText().toString());
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEE, d MMM yyyy HH:mm");
                String format = simpleDateFormat.format(new Date());
                feedback.setTimestamp(format);

                FirebaseDatabase.getInstance().getReference()
                        .child("feedbacks")
                        .child(feedbackId)
                        .setValue(feedback)
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                Toast.makeText(SendFeedBack.this, "Sent", Toast.LENGTH_SHORT).show();
                            }
                        });
            }
        });


    }
}
