package com.tanvir.cse499.diabetestelehealth.models;

public class User {
    private String name;
    private String age;
    private Bglevel bg_level;
    private String spouse_name;
    private String address;
    private String phone_number;
    private String patient_id;
    private String doctor_id;

    public User(String name, String age, Bglevel bg_level, String spouse_name, String address, String phone_number , String patient_id, String doctor_id) {
        this.name = name;
        this.age = age;
        this.bg_level = bg_level;
        this.spouse_name = spouse_name;
        this.address = address;
        this.phone_number = phone_number;
        this.patient_id = patient_id;
        this.doctor_id = doctor_id;
    }

    public User() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public Bglevel getBg_level() {
        return bg_level;
    }

    public void setBg_level(Bglevel bg_level) {
        this.bg_level = bg_level;
    }

    public String getSpouse_name() {
        return spouse_name;
    }

    public void setSpouse_name(String spouse_name) {
        this.spouse_name = spouse_name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getPatient_id() {
        return patient_id;
    }

    public void setPatient_id(String patient_id) {
        this.patient_id = patient_id;
    }

    public String getDoctor_id() {
        return doctor_id;
    }

    public void setDoctor_id(String doctor_id) {
        this.doctor_id = doctor_id;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", age='" + age + '\'' +
                ", bg_level='" + bg_level + '\'' +
                ", spouse_name='" + spouse_name + '\'' +
                ", address='" + address + '\'' +
                ", phone_number='" + phone_number + '\'' +
                ", patient_id='" + patient_id + '\'' +
                ", doctor_id='" + doctor_id + '\'' +
                '}';
    }
}

