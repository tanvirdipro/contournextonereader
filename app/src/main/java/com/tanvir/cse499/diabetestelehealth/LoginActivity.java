package com.tanvir.cse499.diabetestelehealth;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class LoginActivity extends AppCompatActivity {

    private static final String TAG="LoginActivity";
    private EditText email, password;
    private Button login;
    private TextView member;
    private ProgressBar progressBar;
    FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);

        email = (EditText) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.password);
        login = (Button) findViewById(R.id.loginBtn);
        member = (TextView) findViewById(R.id.member);
        firebaseAuth = FirebaseAuth.getInstance();
        progressBar= (ProgressBar) findViewById(R.id.pBarLogin);

        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        final boolean isConnected = networkInfo != null && networkInfo.isConnectedOrConnecting();

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String uMail = email.getText().toString();
                String uPass = password.getText().toString();

                if(isConnected)
                {
                    if (TextUtils.isEmpty(uMail) || TextUtils.isEmpty(uPass))
                        Toast.makeText(getApplicationContext(),"Fill Up the credentials",Toast.LENGTH_LONG).show();

                    if(uPass.length()<6)
                        Toast.makeText(getApplicationContext(),"Password too short",Toast.LENGTH_LONG).show();

                    else{
                        progressBar.setVisibility(View.VISIBLE);
                        Log.d(TAG, "onClick: progressbar visible");
                        loginWithEmail(uMail,uPass);

                    }

                }

            }
        });

        member.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), RegisterActivity.class));
                finish();
            }
        });
    }

    private void loginWithEmail(String email,String pass)
    {
        FirebaseAuth.getInstance().signInWithEmailAndPassword(email,pass)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            Log.d(TAG, "onComplete: Login complete successfully");
                            progressBar.setVisibility(View.INVISIBLE);
                            Intent intent=new Intent(getApplicationContext(),LandingPage.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            finish();

                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                progressBar.setVisibility(View.INVISIBLE);
                Log.d(TAG, "onFailure: Login failed" + e.getMessage());
            }
        });
    }

//    private void checkIfApproved(){
//        DatabaseReference reference=FirebaseDatabase.getInstance().getReference();
//
//        Query checkApproval= reference
//                .child(getString(R.string.patients_node))
//                .orderByKey()
//                .equalTo(FirebaseAuth.getInstance().getCurrentUser().getUid());
//
//        checkApproval.addListenerForSingleValueEvent(new ValueEventListener() {
//            @Override
//            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                for(DataSnapshot singleDataSnapshot: dataSnapshot.getChildren()){
//                    User currentUser= singleDataSnapshot.getValue(User.class);
//                    if(Boolean.parseBoolean(currentUser.getIsApproved())){
//                        startActivity(new Intent(getApplicationContext(),LandingPage.class));
//                        finish();
//                    }
//                    else
//                    {
//                        FirebaseAuth.getInstance().signOut();
//                        displayToast("Your account has not been approved yet");
//                    }
//                }
//            }
//
//            @Override
//            public void onCancelled(@NonNull DatabaseError databaseError) {
//
//            }
//        });
//
//    }

    private void displayToast(String msg){
        Toast.makeText(getApplicationContext(),msg,Toast.LENGTH_LONG).show();
    }
}
