package com.tanvir.cse499.diabetestelehealth;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.tanvir.cse499.diabetestelehealth.models.Doctor;
import com.tanvir.cse499.diabetestelehealth.utils.DoctorListAdapter;

import java.util.ArrayList;
import java.util.Map;

public class DoctorList extends AppCompatActivity {

    private  final String TAG = "DoctorList";
    private  RecyclerView recyclerView;
    private String hospitalId;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor_list);
        hospitalId= getIntent().getExtras().getString("Hospital id");
        recyclerView= (RecyclerView) findViewById(R.id.recyclerViewDoctor);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        getDoctorList();
    }


    private void getDoctorList(){

        DatabaseReference reference=FirebaseDatabase.getInstance().getReference();
        final Query queryDoctors=reference.child("doctors").orderByChild("hospital_id")
                .equalTo(hospitalId);
        Log.d(TAG, "getDoctorList: child node refered");
        queryDoctors.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                ArrayList<Doctor> doctor_list= new ArrayList<>();
                for(DataSnapshot doctorsSnap: dataSnapshot.getChildren()) {
                    Log.d(TAG, "onDataChange: looping through doctor node");
                    Doctor doctor=new Doctor();

                    Map<String ,Object> doctor_info_list= (Map<String, Object>) doctorsSnap.getValue();
                    doctor.setName(doctor_info_list.get("name").toString());
                    Log.d(TAG, "onDataChange: "+doctor.getName());

                    doctor.setDoctor_phone(doctor_info_list.get("doctor_phone").toString());
                    doctor.setDoctor_id(doctor_info_list.get("doctor_id").toString());
                    doctor.setDoctor_email(doctor_info_list.get("doctor_email").toString());
                    doctor.setHospital_id(doctor_info_list.get("hospital_id").toString());
                    doctor.setDesignation(doctor_info_list.get("designation").toString());
                    doctor.setSpecialization(doctor_info_list.get("specialization").toString());
                    Log.d(TAG, "onDataChange: "+doctor.getDoctor_phone());

                    Log.d(TAG, "onDataChange: data set");

                    doctor_list.add(doctor);
                }

                DoctorListAdapter doctorListAdapter= new DoctorListAdapter(doctor_list,DoctorList.this);
                recyclerView.setAdapter(doctorListAdapter);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    @Override
    protected void onResume() {
        checkAuthenticationState();
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }

    private void checkAuthenticationState(){
        Log.d(TAG, "checkAuthenticationState: checking authentication state.");

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        if(user == null){
            Log.d(TAG, "checkAuthenticationState: user is null, navigating back to login screen.");

            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();
        }else{
            Log.d(TAG, "checkAuthenticationState: user is authenticated.");
        }
    }
}

//////////////////////////////














