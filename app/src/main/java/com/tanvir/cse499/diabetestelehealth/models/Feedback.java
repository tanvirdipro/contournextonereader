package com.tanvir.cse499.diabetestelehealth.models;

public class Feedback {
    private String feedback_id;
    private String doctor_name;
    private String timestamp;
    private String patient_id;
    private String feedback;

    public Feedback(String feedback_id, String doctor_name, String timestamp, String patient_id, String feedback) {
        this.feedback_id = feedback_id;
        this.doctor_name = doctor_name;
        this.timestamp = timestamp;
        this.patient_id = patient_id;
        this.feedback = feedback;
    }

    public Feedback() {
    }

    public String getFeedback_id() {
        return feedback_id;
    }

    public void setFeedback_id(String feedback_id) {
        this.feedback_id = feedback_id;
    }

    public String getDoctor_name() {
        return doctor_name;
    }

    public void setDoctor_name(String doctor_name) {
        this.doctor_name = doctor_name;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getPatient_id() {
        return patient_id;
    }

    public void setPatient_id(String patient_id) {
        this.patient_id = patient_id;
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }

    @Override
    public String toString() {
        return "Feedback{" +
                "feedback_id='" + feedback_id + '\'' +
                ", doctor_name='" + doctor_name + '\'' +
                ", timestamp='" + timestamp + '\'' +
                ", patient_id='" + patient_id + '\'' +
                ", feedback='" + feedback + '\'' +
                '}';
    }
}
