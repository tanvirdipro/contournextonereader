package com.tanvir.cse499.diabetestelehealth;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.tanvir.cse499.diabetestelehealth.models.Bglevel;
import com.tanvir.cse499.diabetestelehealth.models.DoctorMsg;
import com.tanvir.cse499.diabetestelehealth.models.User;

public class RegisterActivity extends AppCompatActivity {

    private static final String TAG="RegisterActivity";
    private static final String NULL_VALUE="";
    private EditText email,password;
    private Button signUp,login,doctorReg;
    private FirebaseAuth firebaseAuth;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        progressBar= (ProgressBar) findViewById(R.id.pBar);
        email= (EditText) findViewById(R.id.email);
        password= (EditText) findViewById(R.id.password);
        signUp= (Button) findViewById(R.id.regButton);
        login = (Button) findViewById(R.id.login);
        doctorReg= (Button) findViewById(R.id.sendToDoctorReg);

        // checks if internet is turned on
        ConnectivityManager connectivityManager= (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo= connectivityManager.getActiveNetworkInfo();
        final boolean isConnected= networkInfo!=null && networkInfo.isConnectedOrConnecting();

        firebaseAuth=FirebaseAuth.getInstance();

        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String uMail= email.getText().toString();
                String uPass= password.getText().toString();

                if(isConnected)
                {
                    if (TextUtils.isEmpty(uMail) || TextUtils.isEmpty(uPass))
                        Toast.makeText(getApplicationContext(),"Fill Up the credentials",Toast.LENGTH_LONG).show();

                    if(uPass.length()<6)
                        Toast.makeText(getApplicationContext(),"Password too short",Toast.LENGTH_LONG).show();

                    else{
                        // creates new user in firebase
                        progressBar.setVisibility(View.VISIBLE);
                        registerNewEmail(uMail,uPass);
                    }
                }
                else
                    Toast.makeText(getApplicationContext(),"Turn on Internet",Toast.LENGTH_LONG).show();

            }
        });

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(),LoginActivity.class));
                finish();
            }
        });
        doctorReg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(RegisterActivity.this,DoctorRegistration.class));
                finish();
            }
        });


    }

    // Method for new user registration
    private void registerNewEmail(final String email, String pass)
    {
        FirebaseAuth.getInstance().createUserWithEmailAndPassword(email,pass)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    Log.d(TAG, "onComplete: Registration complete successfully");
                    progressBar.setVisibility(View.INVISIBLE);

                    User user=new User();

                    user.setName(NULL_VALUE);
                    user.setAge(NULL_VALUE);
                    user.setSpouse_name(NULL_VALUE);
                    user.setDoctor_id(NULL_VALUE);
                    user.setAddress(NULL_VALUE);
                    user.setPhone_number(NULL_VALUE);
                    user.setPatient_id(FirebaseAuth.getInstance().getCurrentUser().getUid());


                    FirebaseDatabase.getInstance().getReference()
                            .child(getString(R.string.patients_node))
                            .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                            .setValue(user);

                    FirebaseAuth.getInstance().signOut();
                    startActivity(new Intent(RegisterActivity.this,LoginActivity.class));
                    finish();

                }
                else
                    displayToast(task.getException().getMessage());
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                progressBar.setVisibility(View.INVISIBLE);
            }
        });
    }

    private void displayToast(String msg){
        Toast.makeText(getApplicationContext(),msg,Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        FirebaseUser user= FirebaseAuth.getInstance().getCurrentUser();
        if(user!=null){
            startActivity(new Intent(RegisterActivity.this,LandingPage.class));
            finish();
        }

    }
    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }
}
