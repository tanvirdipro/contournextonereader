package com.tanvir.cse499.diabetestelehealth.models;

public class Bglevel {
    private String bglevel;
    private String timestamp;
    private String bglevelId;

    public Bglevel(String bglevel, String timestamp, String bglevelId) {
        this.bglevel = bglevel;
        this.timestamp = timestamp;
        this.bglevelId = bglevelId;
    }

    public Bglevel() {
    }

    public String getBglevel() {
        return bglevel;
    }

    public void setBglevel(String bglevel) {
        this.bglevel = bglevel;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getBglevelId() {
        return bglevelId;
    }

    public void setBglevelId(String bglevelId) {
        this.bglevelId = bglevelId;
    }

    @Override
    public String toString() {
        return "Bglevel{" +
                "bglevel='" + bglevel + '\'' +
                ", timestamp='" + timestamp + '\'' +
                ", bglevelId='" + bglevelId + '\'' +
                '}';
    }
}

