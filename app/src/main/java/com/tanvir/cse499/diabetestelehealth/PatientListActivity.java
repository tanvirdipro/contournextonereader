package com.tanvir.cse499.diabetestelehealth;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.tanvir.cse499.diabetestelehealth.models.Assign;
import com.tanvir.cse499.diabetestelehealth.models.Bglevel;
import com.tanvir.cse499.diabetestelehealth.models.User;
import com.tanvir.cse499.diabetestelehealth.utils.PatientListAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class PatientListActivity extends AppCompatActivity {
    private static final String TAG = "PatientListActivity";

    DatabaseReference reference= FirebaseDatabase.getInstance().getReference();

    Query query = reference.child("assign").orderByChild("doctor_id")
            .equalTo(FirebaseAuth.getInstance().getCurrentUser().getUid());

    PatientListAdapter patientListAdapter;

    private RecyclerView recyclerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_list);
        recyclerView = (RecyclerView) findViewById(R.id.patientRecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        if(getIntent().getStringExtra("Accepted").equals("Accepted"))
        {
            getPatientList1();
            Log.d(TAG, "onCreate: update1");
            update1();
        }
        else if(getIntent().getStringExtra("Accepted").equals("Pending")){
            getPatientList2();
            Log.d(TAG, "onCreate: update2");
            update2();
        }

    }

    ValueEventListener valueEventListener;

    private void update1(){
        valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                getPatientList1();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };
        reference.child("assign").addValueEventListener(valueEventListener);
    }

    private void update2(){
        valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                getPatientList2();

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };
        reference.child("assign").addValueEventListener(valueEventListener);
    }

    private void getPatientList1(){
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                ArrayList<Assign> assigns = new ArrayList<>();
                for(DataSnapshot snapshots: dataSnapshot.getChildren()) {
                    Assign assign = new Assign();
                    Map<String,Object> assign_list = (Map<String, Object>) snapshots.getValue();
                    assign.setAssign_id(assign_list.get("assign_id").toString());
                    assign.setDoctor_id(assign_list.get("doctor_id").toString());
                    assign.setIsAccepted(assign_list.get("isAccepted").toString());
                    assign.setPatient_id(assign_list.get("patient_id").toString());
                    assigns.add(assign);
                    Log.d(TAG, "onDataChange: PatientList"+assign.getDoctor_id());
                }
                final ArrayList<User> users = new ArrayList<>();
                for(final Assign assign: assigns){
                    reference.child("users")
                            .orderByKey()
                            .equalTo(assign.getPatient_id())
                            .addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    for(DataSnapshot single: dataSnapshot.getChildren()){
                                        User user= new User();
                                        Map<String,Object> user_list = (Map<String, Object>) single.getValue();
                                        user.setBg_level(null);
                                        user.setSpouse_name(user_list.get("spouse_name").toString());
                                        user.setAge(user_list.get("age").toString());
                                        user.setPhone_number(user_list.get("phone_number").toString());
                                        user.setName(user_list.get("name").toString());
                                        user.setPatient_id(user_list.get("patient_id").toString());
                                        user.setAddress(user_list.get("address").toString());
                                        user.setDoctor_id(user_list.get("doctor_id").toString());
                                        if(assign.getIsAccepted().equals("true")){
                                            users.add(user);
                                        }

                                        patientListAdapter= new PatientListAdapter(users,PatientListActivity.this,true);
                                        recyclerView.setAdapter(patientListAdapter);
                                        patientListAdapter.notifyDataSetChanged();
                                    }
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                }
                            });
                }

            }


            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }

        });
    }

    private void getPatientList2(){
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                ArrayList<Assign> assigns = new ArrayList<>();
                for(DataSnapshot snapshots: dataSnapshot.getChildren()) {
                    Assign assign = new Assign();
                    Map<String,Object> assign_list = (Map<String, Object>) snapshots.getValue();
                    assign.setAssign_id(assign_list.get("assign_id").toString());
                    assign.setDoctor_id(assign_list.get("doctor_id").toString());
                    assign.setIsAccepted(assign_list.get("isAccepted").toString());
                    assign.setPatient_id(assign_list.get("patient_id").toString());
                    assigns.add(assign);
                    Log.d(TAG, "onDataChange: PatientList"+assign.getDoctor_id());
                }
                final ArrayList<User> users = new ArrayList<>();
                for(final Assign assign: assigns){
                    reference.child("users")
                            .orderByKey()
                            .equalTo(assign.getPatient_id())
                            .addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    for(DataSnapshot single: dataSnapshot.getChildren()){
                                        User user= new User();
                                        Map<String,Object> user_list = (Map<String, Object>) single.getValue();
                                        user.setBg_level(null);
                                        user.setSpouse_name(user_list.get("spouse_name").toString());
                                        user.setAge(user_list.get("age").toString());
                                        user.setPhone_number(user_list.get("phone_number").toString());
                                        user.setName(user_list.get("name").toString());
                                        user.setPatient_id(user_list.get("patient_id").toString());
                                        user.setAddress(user_list.get("address").toString());
                                        user.setDoctor_id(user_list.get("doctor_id").toString());
                                        if(assign.getIsAccepted().equals("false")){
                                            users.add(user);
                                        }

                                        patientListAdapter= new PatientListAdapter(users,PatientListActivity.this,false);
                                        recyclerView.setAdapter(patientListAdapter);
                                        patientListAdapter.notifyDataSetChanged();
                                    }
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                }
                            });
                }

            }


            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }

        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        reference.child("assign").removeEventListener(valueEventListener);
        Log.d(TAG, "onDestroy: Listener Removed");
    }
}
