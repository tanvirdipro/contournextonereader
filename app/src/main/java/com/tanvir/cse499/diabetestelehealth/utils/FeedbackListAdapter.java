package com.tanvir.cse499.diabetestelehealth.utils;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tanvir.cse499.diabetestelehealth.R;
import com.tanvir.cse499.diabetestelehealth.models.Feedback;

import java.util.List;

public class FeedbackListAdapter extends RecyclerView.Adapter<FeedbackListAdapter.FeedbackViewHolder> {

    private List<Feedback> feedbacks;
    private Context context;

    public FeedbackListAdapter(List<Feedback> feedbacks,Context context){
        this.feedbacks=feedbacks;
        this.context=context;
    }

    @Override
    public FeedbackViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater= LayoutInflater.from(parent.getContext());
        View view= layoutInflater.inflate(R.layout.feedback_list,parent,false);
        return new FeedbackViewHolder(view);
    }

    @Override
    public void onBindViewHolder(FeedbackViewHolder holder, int position) {
        holder.doctorName.setText(feedbacks.get(position).getDoctor_name());
        holder.timestamp.setText(feedbacks.get(position).getTimestamp());
        holder.msg.setText(feedbacks.get(position).getFeedback());
    }

    @Override
    public int getItemCount() {
        return feedbacks.size();
    }

    public class FeedbackViewHolder extends RecyclerView.ViewHolder{

        TextView doctorName,timestamp,msg;
        public FeedbackViewHolder(View itemView) {
            super(itemView);
            doctorName= (TextView) itemView.findViewById(R.id.doctorName);
            timestamp = (TextView) itemView.findViewById(R.id.timestamp);
            msg= (TextView) itemView.findViewById(R.id.feedback);

        }
    }
}
