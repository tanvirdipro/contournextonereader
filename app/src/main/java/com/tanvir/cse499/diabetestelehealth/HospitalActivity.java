package com.tanvir.cse499.diabetestelehealth;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.tanvir.cse499.diabetestelehealth.models.Hospital;
import com.tanvir.cse499.diabetestelehealth.utils.HospitalListAdapter;

import java.util.ArrayList;
import java.util.Map;

public class HospitalActivity extends AppCompatActivity {

    private static final String TAG = "HospitalListActivity";
    private  RecyclerView recyclerView;
    private  ArrayList<Hospital> hospital_list;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hospital);
        recyclerView= (RecyclerView) findViewById(R.id.recyclerViewHospital);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        getHospitalList();
    }



    private void getHospitalList(){

        DatabaseReference reference=FirebaseDatabase.getInstance().getReference();
        final Query query=reference.child("hospitals");
        Log.d(TAG, "getHospitalList: child node refered");
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                ArrayList<Hospital> hospital_list= new ArrayList<>();
                for(DataSnapshot hospitalSnap: dataSnapshot.getChildren()) {
                    Log.d(TAG, "onDataChange: looping through hospital node");
                    Hospital hospital= new Hospital();
                    Map<String ,Object> hospital_info_list= (Map<String, Object>) hospitalSnap.getValue();
                    hospital.setHospital_name(hospital_info_list.get("hospital_name").toString());
                    Log.d(TAG, "onDataChange: "+hospitalSnap.getValue(Hospital.class).getHospital_name());

                    hospital.setHospital_location(hospital_info_list.get("hospital_location").toString());
                    hospital.setHospital_id(hospital_info_list.get("hospital_id").toString());

                    Log.d(TAG, "onDataChange: "+hospital.getHospital_location());

                    Log.d(TAG, "onDataChange: data set");

                    hospital_list.add(hospital);
                }

                HospitalListAdapter hospitalListAdapter= new HospitalListAdapter(hospital_list,HospitalActivity.this);
                recyclerView.setAdapter(hospitalListAdapter);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    @Override
    protected void onResume() {
        checkAuthenticationState();
        super.onResume();
    }

    private void checkAuthenticationState(){
        Log.d(TAG, "checkAuthenticationState: checking authentication state.");

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        if(user == null){
            Log.d(TAG, "checkAuthenticationState: user is null, navigating back to login screen.");

            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();
        }else{
            Log.d(TAG, "checkAuthenticationState: user is authenticated.");
        }
    }


}
