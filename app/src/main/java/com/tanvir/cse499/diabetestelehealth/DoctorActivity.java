package com.tanvir.cse499.diabetestelehealth;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.firebase.auth.FirebaseAuth;

public class DoctorActivity extends AppCompatActivity {

    private Button patientList,patientReq,logOut;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor);
        patientList= (Button) findViewById(R.id.patientList);
        patientReq= (Button) findViewById(R.id.patientReq);
        logOut= (Button) findViewById(R.id.doctorLogout);
        patientList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(DoctorActivity.this,PatientListActivity.class);
                intent.putExtra("Accepted","Accepted");
                startActivity(intent);
            }
        });

        patientReq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(DoctorActivity.this,PatientListActivity.class);
                intent.putExtra("Accepted","Pending");
                startActivity(intent);
            }
        });

        logOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseAuth.getInstance().signOut();
                startActivity(new Intent(DoctorActivity.this,RegisterActivity.class));
                finish();
            }
        });
    }
}
