package com.tanvir.cse499.diabetestelehealth.models;

public class Assign {
    private String doctor_id;
    private String patient_id;
    private String isAccepted;
    private String assign_id;

    public Assign(String doctor_id, String patient_id, String isAccepted, String assign_id) {
        this.doctor_id = doctor_id;
        this.patient_id = patient_id;
        this.isAccepted = isAccepted;
        this.assign_id = assign_id;
    }

    public Assign() {
    }

    public String getDoctor_id() {
        return doctor_id;
    }

    public void setDoctor_id(String doctor_id) {
        this.doctor_id = doctor_id;
    }

    public String getPatient_id() {
        return patient_id;
    }

    public void setPatient_id(String patient_id) {
        this.patient_id = patient_id;
    }

    public String getIsAccepted() {
        return isAccepted;
    }

    public void setIsAccepted(String isAccepted) {
        this.isAccepted = isAccepted;
    }

    public String getAssign_id() {
        return assign_id;
    }

    public void setAssign_id(String assign_id) {
        this.assign_id = assign_id;
    }

    @Override
    public String toString() {
        return "Assign{" +
                "doctor_id='" + doctor_id + '\'' +
                ", patient_id='" + patient_id + '\'' +
                ", isAccepted='" + isAccepted + '\'' +
                ", assign_id='" + assign_id + '\'' +
                '}';
    }
}
