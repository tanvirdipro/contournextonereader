package com.tanvir.cse499.diabetestelehealth.models;

public class DoctorMsg {
    private String msg_body;
    private String msg_time;

    public DoctorMsg(String msg_body, String msg_time) {
        this.msg_body = msg_body;
        this.msg_time = msg_time;
    }

    public DoctorMsg() {
    }

    public String getMsg_body() {
        return msg_body;
    }

    public void setMsg_body(String msg_body) {
        this.msg_body = msg_body;
    }

    public String getMsg_time() {
        return msg_time;
    }

    public void setMsg_time(String msg_time) {
        this.msg_time = msg_time;
    }

    @Override
    public String toString() {
        return "DoctorMsg{" +
                "msg_body='" + msg_body + '\'' +
                ", msg_time='" + msg_time + '\'' +
                '}';
    }
}
