package com.tanvir.cse499.diabetestelehealth;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.tanvir.cse499.diabetestelehealth.models.Feedback;
import com.tanvir.cse499.diabetestelehealth.utils.FeedbackListAdapter;

import java.util.ArrayList;
import java.util.Map;

public class FeedbackActivity extends AppCompatActivity {

    private RecyclerView feedbackView;
    private static final String TAG = "FeedbackActivity";
    DatabaseReference reference = FirebaseDatabase.getInstance().getReference()
                .child("feedbacks");
    Query query= reference.orderByChild("patient_id")
            .equalTo(FirebaseAuth.getInstance().getCurrentUser().getUid());
    ValueEventListener valueEventListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);
        feedbackView= (RecyclerView) findViewById(R.id.recyclerViewFeedback);
        feedbackView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        getFeedbacks();
        listenForFeedback();

    }

    private void listenForFeedback(){
            valueEventListener= new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                getFeedbacks();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };
        reference.addValueEventListener(valueEventListener);
    }

    private void getFeedbacks(){

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                ArrayList<Feedback> feedbacks= new ArrayList<>();
                for(DataSnapshot snapshots: dataSnapshot.getChildren()){


                    Map<String,Object> feedbacList= (Map<String, Object>) snapshots.getValue();
                    Feedback feedback = new Feedback();
                    feedback.setDoctor_name(feedbacList.get("doctor_name").toString());
                    feedback.setTimestamp(feedbacList.get("timestamp").toString());
                    feedback.setFeedback(feedbacList.get("feedback").toString());
                    Log.d(TAG, "onFeedBack: "+feedback.getFeedback());
                    feedback.setFeedback_id(feedbacList.get("feedback_id").toString());
                    feedback.setPatient_id(feedbacList.get("patient_id").toString());
                    feedbacks.add(feedback);
                }
                FeedbackListAdapter feedbackListAdapter = new FeedbackListAdapter(feedbacks,FeedbackActivity.this);
                feedbackView.setAdapter(feedbackListAdapter);
                feedbackListAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        reference.removeEventListener(valueEventListener);
        Log.d(TAG, "onDestroy: Event listener removed");
    }

}
