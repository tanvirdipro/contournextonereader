package com.tanvir.cse499.diabetestelehealth;

import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.tanvir.cse499.diabetestelehealth.models.Bglevel;
import com.tanvir.cse499.diabetestelehealth.services.BGMeterGattService;

import java.text.SimpleDateFormat;
import java.util.Date;

public class MainActivity extends AppCompatActivity   {
    private final static String TAG = MainActivity.class.getSimpleName();
    private final static int REQUEST_ENABLE_BT=1;

    /*
    ----- declare temporary testing button ... delete later
     */

    private Button testData;
    BluetoothAdapter bluetoothAdapter=BluetoothAdapter.getDefaultAdapter();
    Button btnAct;
    TextView bgmac;
    Bundle b;
    public String deviceBTMAC;
    String mDeviceAddress = "00:00:00:00:00:00";
    BGMeterGattService mBGMeterGattService;
    private TextView mConnectionState;
    private boolean mConnected = false;
    private TextView mDataField;
   // ActionBar actionBar;

    // Code to manage Service lifecycle.
    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
            mBGMeterGattService = ((BGMeterGattService.LocalBinder) service).getService();
            if (!mBGMeterGattService.initialize()) {
                Log.e(TAG, "Unable to initialize Bluetooth");
                finish();
            }
            // Automatically connects to the device upon successful start-up initialization.
            mBGMeterGattService.connect(mDeviceAddress);
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mBGMeterGattService = null;
        }
    };

    private final BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if (BGMeterGattService.ACTION_GATT_CONNECTED.equals(action)) {
                mConnected = true;
                updateConnectionState(R.string.connected);
                invalidateOptionsMenu();
            } else if (BGMeterGattService.ACTION_GATT_DISCONNECTED.equals(action)) {
                mConnected = false;
                updateConnectionState(R.string.disconnected);
                invalidateOptionsMenu();
            } else if (BGMeterGattService.ACTION_DATA_AVAILABLE.equals(action)) {
                displayData(intent.getStringExtra(BGMeterGattService.EXTRA_DATA),
                        intent.getExtras().getDouble("Bg level"));
            }
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //actionBar= getActionBar();
        //actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("1C2331")));
        btnAct = (Button) findViewById(R.id.listpaireddevices);
        bgmac = (TextView)findViewById(R.id.bgmac);
        btnAct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!bluetoothAdapter.isEnabled())
                {
                    Intent enableBT = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                    startActivityForResult(enableBT, REQUEST_ENABLE_BT);
                    //onActivityResult();
                }
                else {
                    Intent intent = new Intent (getApplicationContext(), BGMeterActivity.class);
                    startActivity (intent);
                }
            }
        });
        mConnectionState = (TextView) findViewById(R.id.connection_state);
        Intent gattServiceIntent = new Intent(this, BGMeterGattService.class);
        bindService(gattServiceIntent, mServiceConnection, BIND_AUTO_CREATE);
        mDataField = (TextView) findViewById(R.id.bgreading);

        try {
            b = getIntent().getExtras();
            deviceBTMAC = b.getString("BG Meter MAC Address");
            Log.v("deviceBTMAC", deviceBTMAC);
            bgmac.setText("BGMeter MAC: " + deviceBTMAC);
            mDeviceAddress = deviceBTMAC;
        }
        catch (Exception e) {
            Log.v("try_catch", "Error " + e.getMessage());
        }
    }

    private void uploadBgData(final double bglevel){
        AlertDialog.Builder uploadDialog= new AlertDialog.Builder(this);
        uploadDialog.setTitle("Upload data");
        uploadDialog.setMessage("Do you want to upload this data?");
        uploadDialog.setPositiveButton("Upload",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy-hh-mm-ss");
                        String format = simpleDateFormat.format(new Date());

                        DatabaseReference ref=FirebaseDatabase.getInstance().getReference();

                        String bgId= ref.child("users")
                                .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                                .child("bg_level")
                                .push().getKey();
                        ref.child("users")
                        .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                        .child("bg_level")
                                .child(bgId)
                                .setValue(new Bglevel(String.valueOf(bglevel),format,bgId))
                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        Toast.makeText(getApplicationContext(),
                                                "Data Uploaded successfully"
                                                ,Toast.LENGTH_SHORT).show();
                                    }
                                });
                    }
                });
        uploadDialog.setNegativeButton("Discard", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        uploadDialog.setCancelable(false);
        uploadDialog.create().show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilter());
        if (mBGMeterGattService != null) {
            final boolean result = mBGMeterGattService.connect(mDeviceAddress);
            Log.d(TAG, "Connect request result=" + result);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(mGattUpdateReceiver);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbindService(mServiceConnection);
        mBGMeterGattService = null;
    }

    private void displayData(String data,Double bglevel) {
        if (data != null) {
            mDataField.setText(data);
        }
        uploadBgData(bglevel);
    }

    private static IntentFilter makeGattUpdateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BGMeterGattService.ACTION_GATT_CONNECTED);
        intentFilter.addAction(BGMeterGattService.ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(BGMeterGattService.ACTION_DATA_AVAILABLE);
        return intentFilter;
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode==RESULT_OK){
            Intent intent = new Intent (this, BGMeterActivity.class);
            startActivity (intent);
        }
    }

    private void updateConnectionState(final int resourceId) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mConnectionState.setText(resourceId);
            }
        });
    }


}