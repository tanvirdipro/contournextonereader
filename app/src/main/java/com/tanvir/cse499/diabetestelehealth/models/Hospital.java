package com.tanvir.cse499.diabetestelehealth.models;

public class Hospital {

    private String hospital_id;
    private String hospital_location;
    private String hospital_name;

    public Hospital(String hospital_id, String hospital_location, String hospital_name) {
        this.hospital_id = hospital_id;
        this.hospital_location = hospital_location;
        this.hospital_name = hospital_name;
    }

    public Hospital() {
    }

    public String getHospital_id() {
        return hospital_id;
    }

    public void setHospital_id(String hospital_id) {
        this.hospital_id = hospital_id;
    }

    public String getHospital_location() {
        return hospital_location;
    }

    public void setHospital_location(String hospital_location) {
        this.hospital_location = hospital_location;
    }

    public String getHospital_name() {
        return hospital_name;
    }

    public void setHospital_name(String hospital_name) {
        this.hospital_name = hospital_name;
    }
}
