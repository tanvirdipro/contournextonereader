package com.tanvir.cse499.diabetestelehealth.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;
import com.tanvir.cse499.diabetestelehealth.LandingPage;
import com.tanvir.cse499.diabetestelehealth.R;
import com.tanvir.cse499.diabetestelehealth.models.Assign;
import com.tanvir.cse499.diabetestelehealth.models.Doctor;
import com.tanvir.cse499.diabetestelehealth.models.Hospital;
import com.tanvir.cse499.diabetestelehealth.models.User;

import java.util.List;
import java.util.Map;

public class DoctorListAdapter extends RecyclerView.Adapter<DoctorListAdapter.DoctorViewHolder> {

    List<Doctor> doctors;
    private Context context;
    String doctor_id_assign;
    private static final String NULL_VALUE="";
    private static final String TAG = "DoctorListAdapter";

    public DoctorListAdapter() {
    }

    public DoctorListAdapter(List<Doctor> doctors, Context context)
    {
        this.doctors = doctors;
        this.context=context;
    }

    @Override
    public DoctorViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater=LayoutInflater.from(parent.getContext());
        View view= layoutInflater.inflate(R.layout.doctor_list, parent, false);
        return new DoctorViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final DoctorViewHolder holder, final int position) {

        final DatabaseReference reference= FirebaseDatabase.getInstance().getReference();
        Query query= reference.child("hospitals")
                .orderByKey()
                .equalTo(doctors.get(position).getHospital_id());
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(final DataSnapshot snap: dataSnapshot.getChildren())
                {
                    Hospital hospital = new Hospital();
                    Map<String ,Object> hospital_info_list= (Map<String, Object>) snap.getValue();
                    hospital.setHospital_name(hospital_info_list.get("hospital_name").toString());
                    hospital.setHospital_location(hospital_info_list.get("hospital_location").toString());
                    hospital.setHospital_id(hospital_info_list.get("hospital_id").toString());
                    Picasso.get().load("http://www.myiconfinder.com/uploads/iconsets/2e92d9a5a3578e4335baf355ba203ad9-plus.png").into(holder.add);
                    Picasso.get().load("https://img.freepik.com/free-vector/doctor-character-background_1270-84.jpg?size=338&ext=jpg").into(holder.image);
                    holder.nameView.setText("Doctor Name: "+doctors.get(position).getName());
                    holder.clinicView.setText("Hospital Name: "+hospital_info_list.get("hospital_name").toString());
                    holder.designationView.setText("Designation: "+doctors.get(position).getDesignation());
                    holder.specializationView.setText("Specialization: "+doctors.get(position).getSpecialization());
                    holder.add.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            final AlertDialog.Builder  addDialogue= new AlertDialog.Builder(context);
                            addDialogue.setIcon(R.drawable.ic_notification);
                            addDialogue.setTitle("Add Doctor");
                            addDialogue.setMessage("ADD THIS DOCTOR??");

                            addDialogue.setNegativeButton("Discard",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                        }
                                    });


                            addDialogue.setPositiveButton("Save",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            Query checkDoc= reference.child("users")
                                                    .orderByKey()
                                                    .equalTo(FirebaseAuth.getInstance().getCurrentUser().getUid());
                                            checkDoc.addListenerForSingleValueEvent(new ValueEventListener() {
                                                @Override
                                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                    for(DataSnapshot snapshot: dataSnapshot.getChildren())
                                                    {
                                                        Log.d(TAG, "onDataChange: entered into for loop");
                                                        User user= snapshot.getValue(User.class);
                                                        if(user.getDoctor_id().equals(""))
                                                        {
                                                            doctor_id_assign=doctors.get(position).getDoctor_id();
                                                            final String assignId= reference.child("assign").push().getKey();
                                                            Assign assignDoctor= new Assign();
                                                            assignDoctor.setAssign_id(assignId);
                                                            assignDoctor.setDoctor_id(doctor_id_assign);
                                                            assignDoctor.setPatient_id(FirebaseAuth.getInstance().getCurrentUser().getUid());
                                                            assignDoctor.setIsAccepted("false");
                                                            reference.child("assign").child(assignId).setValue(assignDoctor)
                                                                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                                                                        @Override
                                                                        public void onSuccess(Void aVoid) {
                                                                            Toast.makeText(context, "Add doctor request sent", Toast.LENGTH_SHORT).show();
                                                                            reference.child("users").child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                                                                                    .child("doctor_id").setValue(assignId);
                                                                        }
                                                                    });
                                                        }
                                                        else
                                                            Toast.makeText(context, "You can only add/request single doctor", Toast.LENGTH_SHORT).show();
                                                    }
                                                }

                                                @Override
                                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                                }
                                            });


//                                            DatabaseReference ref= FirebaseDatabase.getInstance().getReference();
//                                            Query q= ref.child("users")
//                                                    .orderByKey()
//                                                    .equalTo(FirebaseAuth.getInstance().getCurrentUser().getUid());
//                                            q.addListenerForSingleValueEvent(new ValueEventListener() {
//                                                @Override
//                                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                                                    for(DataSnapshot snap: dataSnapshot.getChildren())
//                                                    {
//                                                        User user = snap.getValue(User.class);
//                                                        if(TextUtils.isEmpty(user.getDoctor_id())){
//                                                            FirebaseDatabase.getInstance().getReference()
//                                                                    .child("users")
//                                                                    .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
//                                                                    .child("doctor_id")
//                                                                    .setValue(doctor_id_assign)
//                                                                    .addOnSuccessListener(new OnSuccessListener<Void>() {
//                                                                        @Override
//                                                                        public void onSuccess(Void aVoid) {
//                                                                            Log.d(TAG, "onSuccess: doctor added ");
//                                                                            Toast.makeText(context," assigned",Toast.LENGTH_SHORT).show();
//                                                                            Intent intent=new Intent(context,LandingPage.class);
//                                                                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                                                                            context.startActivity(intent);
//                                                                        }
//                                                                    }).addOnFailureListener(new OnFailureListener() {
//                                                                @Override
//                                                                public void onFailure(@NonNull Exception e) {
//                                                                    Toast.makeText(context,"Already assigned",Toast.LENGTH_SHORT).show();
//                                                                }
//                                                            });
//                                                        }
//                                                        else{
//                                                            Toast.makeText(context,"You already have a doctor assigned",Toast.LENGTH_SHORT).show();
//                                                        }
//                                                    }
//                                                }
//
//                                                @Override
//                                                public void onCancelled(@NonNull DatabaseError databaseError) {
//
//                                                }
//                                            });
                                        }
                                    });
                            addDialogue.create().show();
                        }
                    });

                    holder.parentLayout.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Log.d(TAG, "onClick: "+ holder.nameView.getText());
                        }
                    });
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return doctors.size();
    }

    public class DoctorViewHolder extends RecyclerView.ViewHolder {

        TextView nameView,clinicView,designationView,specializationView;
        CardView parentLayout;
        ImageView image,add;
        public DoctorViewHolder(View itemView) {
            super(itemView);
            parentLayout= (CardView) itemView.findViewById(R.id.cardView);
            nameView= (TextView) itemView.findViewById(R.id.name);
            clinicView=(TextView) itemView.findViewById(R.id.clinic);
            designationView=(TextView) itemView.findViewById(R.id.designation);
            specializationView=(TextView) itemView.findViewById(R.id.specialization);
            add= (ImageView) itemView.findViewById(R.id.addDoc);
            image =(ImageView)itemView.findViewById(R.id.image);

        }
    }

}
