package com.tanvir.cse499.diabetestelehealth;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class DoctorLogin extends AppCompatActivity {
    private static final String TAG="LoginActivity";
    private EditText email, password;
    private Button login;
    private ProgressBar progressBar;
    FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor_login);
        email = (EditText) findViewById(R.id.doctorEmail);
        password = (EditText) findViewById(R.id.doctorPassword);
        login = (Button) findViewById(R.id.dloginBtn);
        firebaseAuth = FirebaseAuth.getInstance();
        progressBar= (ProgressBar) findViewById(R.id.dBarLogin);

        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        final boolean isConnected = networkInfo != null && networkInfo.isConnectedOrConnecting();

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String uMail = email.getText().toString();
                String uPass = password.getText().toString();

                if(isConnected)
                {
                    if (TextUtils.isEmpty(uMail) || TextUtils.isEmpty(uPass))
                        Toast.makeText(getApplicationContext(),"Fill Up the credentials",Toast.LENGTH_LONG).show();

                    if(uPass.length()<6)
                        Toast.makeText(getApplicationContext(),"Password too short",Toast.LENGTH_LONG).show();

                    else{
                        progressBar.setVisibility(View.VISIBLE);
                        Log.d(TAG, "onClick: progressbar visible");
                        loginWithEmail(uMail,uPass);

                    }

                }

            }
        });
    }

    private void loginWithEmail(String email,String pass)
    {
        FirebaseAuth.getInstance().signInWithEmailAndPassword(email,pass)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            Log.d(TAG, "onComplete: Login complete successfully");
                            progressBar.setVisibility(View.INVISIBLE);
                            Intent intent=new Intent(getApplicationContext(),DoctorActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            finish();

                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                progressBar.setVisibility(View.INVISIBLE);
                Log.d(TAG, "onFailure: Login failed" + e.getMessage());
            }
        });
    }
}
