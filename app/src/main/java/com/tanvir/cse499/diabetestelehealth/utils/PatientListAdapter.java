package com.tanvir.cse499.diabetestelehealth.utils;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;
import com.tanvir.cse499.diabetestelehealth.DataHistory;
import com.tanvir.cse499.diabetestelehealth.R;
import com.tanvir.cse499.diabetestelehealth.models.User;

import java.util.List;

public class PatientListAdapter extends RecyclerView.Adapter<PatientListAdapter.PatientViewHolder> {

    private List<User> users;
    private Context context;
    Boolean isAccepted;

    public PatientListAdapter(List<User> users, Context context) {
        this.users = users;
        this.context = context;
    }

    public PatientListAdapter(List<User> users, Context context,Boolean isAccepted) {
        this.users = users;
        this.context = context;
        this.isAccepted= isAccepted;
    }

    @Override
    public PatientViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.patient_list,parent,false);

        return new PatientViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PatientViewHolder holder, final int position) {
        Picasso.get().load("http://artistsvalley.com/images/icons/Medical%20Healthcare%20Icons/Patient%20Diagnosis%20Group/256x256/Patient%20Diagnosis%20Group.jpg").into(holder.patientImage); // insert image link
        holder.patientName.setText(users.get(position).getName());
        holder.patientAge.setText(users.get(position).getAge());
        holder.patientPhone.setText(users.get(position).getPhone_number());
        holder.patientSpouse.setText(users.get(position).getSpouse_name());
        if(isAccepted){
            Picasso.get().load("https://cdn1.iconfinder.com/data/icons/round-ui/123/47-512.png").into(holder.addPatient);
            holder.addPatient.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    FirebaseDatabase.getInstance().getReference()
                            .child("assign")
                            .child(users.get(position).getDoctor_id())
                            .child("isAccepted")
                            .setValue("false");
                }
            });
            holder.cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent= new Intent(context,DataHistory.class);
                    intent.putExtra("patient_id",users.get(position).getPatient_id());
                    context.startActivity(intent);
                }
            });
        }
        else
        {
            Picasso.get().load("http://www.myiconfinder.com/uploads/iconsets/2e92d9a5a3578e4335baf355ba203ad9-plus.png").into(holder.addPatient);
            holder.addPatient.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    FirebaseDatabase.getInstance().getReference()
                    .child("assign")
                    .child(users.get(position).getDoctor_id())
                    .child("isAccepted")
                    .setValue("true");
                }
            });
            holder.cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(context, "Accept First to view Details", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    public class PatientViewHolder extends RecyclerView.ViewHolder{

        private ImageView patientImage,addPatient;
        private CardView cardView;
        private TextView patientName,patientAge,patientPhone,patientSpouse;
        public PatientViewHolder(View itemView) {
            super(itemView);
            patientImage= (ImageView) itemView.findViewById(R.id.patientImage);
            patientName= (TextView) itemView.findViewById(R.id.patientName);
            patientAge = (TextView) itemView.findViewById(R.id.patientAge);
            patientPhone= (TextView) itemView.findViewById(R.id.patientPhone);
            patientSpouse= (TextView) itemView.findViewById(R.id.patientSpouse);
            cardView= (CardView) itemView.findViewById(R.id.patientCardView);
            addPatient= (ImageView) itemView.findViewById(R.id.addPatient);
        }
    }
}
